## Кодирует в картинку текст и обратно

Позволяет записать текст в картинку в 2 младших бита цвета. При условии, что цвет пишется в 8 бит, то 2 мита - это максимум 3/255 что примерно равно 1%. При этом глаз человека не может отличить яркость двух источников, если они отличаются менее чем на 1%.

Для справки выполните `go run ./cmd/main.go help` или `imgencoder.exe help` если используети собранный бинарник

Может записывать/читать текст в картинку из консоли/файла. Так же по команде `capacity` можно получить вместимость в байтах
