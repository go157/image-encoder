#!/bin/bash
set -x

initProject() {
    go get -d ./...
}


if [[ $CI_JOB_NAME == "lint" ]]
then
    initProject
    golangci-lint run
elif [[ $CI_JOB_NAME == "test" ]]
then
    go test ./...
fi
