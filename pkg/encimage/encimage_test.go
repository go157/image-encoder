package encimage_test

import (
	"image"
	"io"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ffsaschagff/imgencoder/pkg/encimage"
	"syreclabs.com/go/faker"
)

func TestImage_Write(t *testing.T) {
	t.Parallel()

	type testCase struct {
		name          string
		testImageSize int
		toWrite       []string
		wantText      string
		wantErrWrite  error
		wantErrRead   error
	}
	tests := []testCase{
		{
			name:          "write 3 str",
			testImageSize: 50,
			toWrite: []string{
				"Строка 1",
				" и Сторка 2",
				" и Сторка 3",
			},
			wantText: "Строка 1 и Сторка 2 и Сторка 3",
		},
		func() testCase {
			fullStr := faker.RandomString(1250)
			return testCase{
				name:          "write full capacity",
				testImageSize: 50,
				toWrite: []string{
					fullStr,
				},
				wantText: fullStr,
			}
		}(),
		{
			name:          "lorem ipsum",
			testImageSize: 1000,
			toWrite: []string{
				`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac libero nisi. Nam suscipit metus mauris, a suscipit ex mattis vitae. Ut eu tempor massa. Aenean id sem hendrerit, volutpat enim vitae, commodo neque. Proin cursus dui quis massa porttitor, eget ullamcorper mi consequat. Morbi dignissim nisi vel blandit convallis. Praesent augue nibh, feugiat ac erat at, pellentesque hendrerit arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec sagittis maximus quam in ultrices. Nulla bibendum eros sed eros commodo elementum. Proin iaculis eget nunc imperdiet blandit. Nam sed bibendum lectus. Nam egestas, arcu ut mollis bibendum, urna augue consectetur turpis, a euismod sem sem in elit. Suspendisse potenti. In a elementum enim.

			Vestibulum sollicitudin urna eu odio consequat euismod. Proin eu urna leo. Donec erat lectus, pretium at lobortis sit amet, scelerisque ut elit. Fusce fermentum nibh eget orci venenatis blandit. Ut eu convallis leo. Aenean eu justo posuere, interdum erat eget, placerat ex. Cras aliquam vestibulum felis, non vestibulum magna laoreet vitae. In vehicula sem vestibulum magna blandit malesuada. Mauris ligula odio, tincidunt sit amet porta vitae, blandit eget augue. Aliquam rhoncus ex sed nisi varius tincidunt. Vestibulum nisi est, molestie in rhoncus vel, euismod sit amet eros. Nulla vel quam nunc. In hac habitasse platea dictumst. Cras mauris elit, viverra a metus et, imperdiet sollicitudin lectus. Nulla rhoncus aliquet leo non hendrerit.
			
			Nunc in urna lorem. Vivamus mattis massa nec leo porttitor ultricies. Aenean et aliquam magna. Suspendisse vitae sagittis mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate ex urna, in ultricies sapien porta sit amet. Donec ut libero vitae neque ullamcorper egestas. Phasellus posuere, augue id lacinia lobortis, orci orci mollis arcu, vitae accumsan eros massa eu enim. Curabitur fringilla ipsum a risus fermentum cursus. Sed eget leo odio.
			
			Vestibulum congue sodales magna eget semper. Nulla pretium malesuada tincidunt. Mauris ut faucibus metus. Praesent dictum at velit in porttitor. Mauris a finibus sapien. Donec vitae odio lacus. Ut consequat, lorem malesuada euismod dignissim, sapien sapien elementum neque, in feugiat purus magna vel nibh. Integer felis nisi, mollis sed metus ac, egestas rutrum neque. Integer tempor odio iaculis massa accumsan, luctus vestibulum orci vestibulum. Phasellus varius ac nisl ultrices volutpat.
			
			Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur dignissim risus ac neque maximus, a auctor libero commodo. Nam commodo pellentesque lectus, in porta ligula accumsan eu. Nulla sit amet sem id magna pulvinar euismod. Morbi sagittis elit quis orci cursus accumsan. Ut ut semper risus. Donec vulputate nibh a porta interdum. Praesent consectetur mollis nisl, a iaculis nisi aliquet vel. Proin vel elementum eros, eget dapibus est. Praesent feugiat eros in mauris hendrerit, dictum vulputate ligula tristique. Nulla massa sapien, maximus quis porttitor vel, blandit id ligula. Nam iaculis pellentesque fermentum.`,
			},
			wantText: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac libero nisi. Nam suscipit metus mauris, a suscipit ex mattis vitae. Ut eu tempor massa. Aenean id sem hendrerit, volutpat enim vitae, commodo neque. Proin cursus dui quis massa porttitor, eget ullamcorper mi consequat. Morbi dignissim nisi vel blandit convallis. Praesent augue nibh, feugiat ac erat at, pellentesque hendrerit arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec sagittis maximus quam in ultrices. Nulla bibendum eros sed eros commodo elementum. Proin iaculis eget nunc imperdiet blandit. Nam sed bibendum lectus. Nam egestas, arcu ut mollis bibendum, urna augue consectetur turpis, a euismod sem sem in elit. Suspendisse potenti. In a elementum enim.

			Vestibulum sollicitudin urna eu odio consequat euismod. Proin eu urna leo. Donec erat lectus, pretium at lobortis sit amet, scelerisque ut elit. Fusce fermentum nibh eget orci venenatis blandit. Ut eu convallis leo. Aenean eu justo posuere, interdum erat eget, placerat ex. Cras aliquam vestibulum felis, non vestibulum magna laoreet vitae. In vehicula sem vestibulum magna blandit malesuada. Mauris ligula odio, tincidunt sit amet porta vitae, blandit eget augue. Aliquam rhoncus ex sed nisi varius tincidunt. Vestibulum nisi est, molestie in rhoncus vel, euismod sit amet eros. Nulla vel quam nunc. In hac habitasse platea dictumst. Cras mauris elit, viverra a metus et, imperdiet sollicitudin lectus. Nulla rhoncus aliquet leo non hendrerit.
			
			Nunc in urna lorem. Vivamus mattis massa nec leo porttitor ultricies. Aenean et aliquam magna. Suspendisse vitae sagittis mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate ex urna, in ultricies sapien porta sit amet. Donec ut libero vitae neque ullamcorper egestas. Phasellus posuere, augue id lacinia lobortis, orci orci mollis arcu, vitae accumsan eros massa eu enim. Curabitur fringilla ipsum a risus fermentum cursus. Sed eget leo odio.
			
			Vestibulum congue sodales magna eget semper. Nulla pretium malesuada tincidunt. Mauris ut faucibus metus. Praesent dictum at velit in porttitor. Mauris a finibus sapien. Donec vitae odio lacus. Ut consequat, lorem malesuada euismod dignissim, sapien sapien elementum neque, in feugiat purus magna vel nibh. Integer felis nisi, mollis sed metus ac, egestas rutrum neque. Integer tempor odio iaculis massa accumsan, luctus vestibulum orci vestibulum. Phasellus varius ac nisl ultrices volutpat.
			
			Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur dignissim risus ac neque maximus, a auctor libero commodo. Nam commodo pellentesque lectus, in porta ligula accumsan eu. Nulla sit amet sem id magna pulvinar euismod. Morbi sagittis elit quis orci cursus accumsan. Ut ut semper risus. Donec vulputate nibh a porta interdum. Praesent consectetur mollis nisl, a iaculis nisi aliquet vel. Proin vel elementum eros, eget dapibus est. Praesent feugiat eros in mauris hendrerit, dictum vulputate ligula tristique. Nulla massa sapien, maximus quis porttitor vel, blandit id ligula. Nam iaculis pellentesque fermentum.`,
		},
		{
			name:          "out of capacity",
			testImageSize: 5,
			toWrite: []string{
				"1234567890qwertyuiop[]asdfghjkl;'zxcvbnm,./",
			},
			wantText:     "",
			wantErrWrite: encimage.ErrCapIsLow,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			//t.Parallel()

			encImage := encimage.New(image.NewRGBA(image.Rect(0, 0, tt.testImageSize, tt.testImageSize)))
			var err error
			for _, oneToWrite := range tt.toWrite {
				_, err = encImage.Write([]byte(oneToWrite))
				if err != nil {
					break
				}
			}
			require.ErrorIs(t, tt.wantErrWrite, err)

			buff, err := io.ReadAll(&encImage)
			require.ErrorIs(t, tt.wantErrRead, err)
			require.Equal(t, tt.wantText, string(buff))
		})
	}
}
