package encimage

import (
	"errors"
	"image"
	"io"
	"math"
)

type Image struct {
	pixels     []*Pixel
	height     int
	width      int
	lastReaded int
}

type Pixel struct {
	red   uint32
	green uint32
	blue  uint32
}

// meaningBytes - lower bytes to write data.
const meaningBytes = 2

var ErrCapIsLow = errors.New("capacity is too low")

// New - get image from source and init it (warning: it's reset all already written data).
func New(image image.Image) Image {
	result := GetPixels(image)
	result.InitToWrite()

	return result
}

// GetPixels - return image avilabe to write data.
func GetPixels(image image.Image) Image {
	bounds := image.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	result := Image{
		pixels: make([]*Pixel, height*width),
		height: height,
		width:  width,
	}

	// Represent pixels 2D array as 1D array
	// 1 2 3
	// 4 5 6 => 1 2 3 4 5 6 7 8 9
	// 7 8 9
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			red, green, blue, _ := image.At(x, y).RGBA()
			result.pixels[x+y*width] = &Pixel{
				red:   red,
				green: green,
				blue:  blue,
			}
		}
	}

	return result
}

// Read - Readed interface implementation.
func (encimage *Image) Read(buff []byte) (int, error) {
	var byteIndex int
	var pixelIndex int
	pixelsForByte := pixelsForByte()
	capacity := encimage.GetCapacity()
	// iterate slice by enough pixels to get byte.
	for ; byteIndex < len(buff); byteIndex++ {
		if len(buff)-1 < byteIndex {
			return byteIndex, nil
		}
		// read one byte.
		pixelIndex = encimage.lastReaded + byteIndex
		if pixelIndex >= capacity {
			break
		}
		oneByte, ok := getData(encimage.pixels[pixelsForByte*pixelIndex : pixelsForByte*(pixelIndex+1)])
		// if control is false it must be EOF.
		if !ok {
			encimage.lastReaded += byteIndex

			return byteIndex, io.EOF
		}
		buff[byteIndex] = oneByte
	}

	encimage.lastReaded += byteIndex
	var result error
	if pixelIndex >= capacity {
		result = io.EOF
	}

	return byteIndex, result
}

// Write - Writer interface implementation.
func (encimage Image) Write(data []byte) (int, error) {
	// get last written byte index to write after that
	lastByteIndex := encimage.getLastByteIndex()
	capacity := encimage.GetCapacity()

	if len(data)+lastByteIndex > capacity {
		return 0, ErrCapIsLow
	}

	pixelsForByte := pixelsForByte()
	// iterate slice by enough pixels to write byte.
	var byteIndex int
	for ; byteIndex < capacity; byteIndex++ {
		// if there no more bytes we will write incorrect control.
		pixelIndex := lastByteIndex + byteIndex
		if len(data)-1 < byteIndex {
			setDataEnd(encimage.pixels[pixelsForByte*pixelIndex : pixelsForByte*(pixelIndex+1)])

			break
		}
		// write one byte.
		setData(encimage.pixels[pixelsForByte*pixelIndex:pixelsForByte*(pixelIndex+1)], data[byteIndex])
	}

	return byteIndex, nil
}

// GetImage - get image from writable image.
func (encimage Image) GetImage() image.Image {
	result := image.NewRGBA(image.Rect(0, 0, encimage.width, encimage.height))
	for position, pixel := range encimage.pixels {
		result.Pix[position*4] = uint8(pixel.red)
		result.Pix[1+position*4] = uint8(pixel.green)
		result.Pix[2+position*4] = uint8(pixel.blue)
		result.Pix[3+position*4] = 255
	}

	return result
}

// GetCapacity - how many bytes we can write into image.
func (encimage Image) GetCapacity() int {
	return len(encimage.pixels) / pixelsForByte()
}

// InitToWrite - init zero control pixel to invalid value to make shure we start from beginning.
func (encimage Image) InitToWrite() {
	pixel := encimage.pixels[0]
	mask := uint32(math.Pow(2, float64(meaningBytes)) - 1)
	addGreen := (pixel.green & mask)
	addRed := (pixel.red & mask)
	addBlue := addRed ^ addGreen + 1
	pixel.blue = (pixel.blue &^ mask) + addBlue
}

func getData(pixels []*Pixel) (byte, bool) {
	var oneByte byte
	mask := uint32(math.Pow(2, float64(meaningBytes)) - 1)

	if !pixels[0].isControlPixel() {
		return oneByte, false
	}

	for bytePieceIndex, pixel := range pixels {
		if bytePieceIndex == 0 {
			oneByte = byte(pixel.red & mask)
		} else {
			oneByte = (oneByte << meaningBytes) + byte(pixel.red&mask)
		}
		oneByte = (oneByte << meaningBytes) + byte(pixel.green&mask)
	}

	return oneByte, true
}

func setData(pixels []*Pixel, data byte) {
	// write in revers to reading.
	mask := uint32(math.Pow(2, float64(meaningBytes)) - 1)
	for index := pixelsForByte() - 1; index >= 0; index-- {
		addGreen := (uint32(data) & mask)
		pixels[index].green = (pixels[index].green &^ mask) + addGreen
		data >>= meaningBytes
		addRed := (uint32(data) & mask)
		pixels[index].red = (pixels[index].red &^ mask) + addRed
		data >>= meaningBytes
		// control
		if index == 0 {
			addBlue := addRed ^ addGreen
			pixels[index].blue = (pixels[index].blue &^ mask) + addBlue
		}
	}
}

func setDataEnd(pixels []*Pixel) {
	// write incorrect control
	mask := uint32(math.Pow(2, float64(meaningBytes)) - 1)

	addGreen := (pixels[0].green & mask)
	addRed := (pixels[0].red & mask)
	addBlue := ((addRed ^ addGreen) + 1) & mask
	pixels[0].blue = (pixels[0].blue &^ mask) + addBlue
}

func (encimage Image) getLastByteIndex() int {
	var lastByteIndex int
	for index := 0; index < encimage.GetCapacity(); index++ {
		pfb := pixelsForByte()
		pixel := encimage.pixels[index*pfb]

		if !pixel.isControlPixel() {
			break
		}
		lastByteIndex++
	}

	return lastByteIndex
}

func pixelsForByte() int {
	return 8 / (2 * meaningBytes)
}

func (pixel Pixel) isControlPixel() bool {
	mask := uint32(math.Pow(2, float64(meaningBytes)) - 1)
	addGreen := (pixel.green & mask)
	addRed := (pixel.red & mask)
	addBlue := addRed ^ addGreen

	return (pixel.blue & mask) == addBlue
}
