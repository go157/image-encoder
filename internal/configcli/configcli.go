package configcli

import (
	"errors"
	"image"
	"image/png"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/ffsaschagff/imgencoder/pkg/encimage"
)

// Config - description of command to run.
type Config struct {
	Command Command
}

// Command of cli - when you print it and press enter it will do Do().
type Command interface {
	Do()
}

// Help - command for help.
type Help struct {
	command string
}

// Decode - command for get []bytes from png.
type Decode struct {
	path    string
	output  string
	message []byte
}

// Decode - command for get capacity of png.
type Capacity struct {
	path string
}

// Encode - command for write []bytes to png.
type Encode struct {
	path    string
	input   string
	output  string
	message []byte
}

type ErrorMsg struct {
	message string
}

// GetConfig - transform os.Args to Config.
func GetConfig() Config {
	config := Config{}
	args := os.Args
	if len(args) == 1 {
		config.Command = Help{}

		return config
	}

	switch args[1] {
	case "decode":
		config.Command = getDecode(args)
	case "capacity":
		config.Command = getCapacity(args)
	case "encode":
		config.Command = getEncode(args)
	default:
		config.Command = getHelp(args)
	}

	return config
}

func getDecode(args []string) Command {
	if len(args) <= 2 {
		return ErrorMsg{message: "need path"}
	}
	path := args[2]

	startPos := 3
	output := ""
	for indexArgs := 3; indexArgs < len(args); indexArgs++ {
		if strings.Index(args[indexArgs], "-o=") == 0 {
			startPos++
			output = args[indexArgs][3:]
		}
	}

	return Decode{
		path:   path,
		output: output,
	}
}

func getCapacity(args []string) Command {
	if len(args) <= 2 {
		return ErrorMsg{message: "need path"}
	}
	path := args[2]

	return Capacity{
		path: path,
	}
}

func getEncode(args []string) Command {
	if len(args) <= 3 {
		return ErrorMsg{message: "need path and message"}
	}
	startPos := 3
	output := "result.png"
	input := ""
	for indexArgs := 3; indexArgs < len(args); indexArgs++ {
		switch {
		case strings.Index(args[indexArgs], "-o=") == 0:
			startPos++
			output = args[indexArgs][3:]
		case strings.Index(args[indexArgs], "-i=") == 0:
			startPos++
			input = args[indexArgs][3:]
		}
	}
	message := strings.Join(args[startPos:], " ")

	return Encode{
		path:    args[2],
		message: []byte(message),
		output:  output,
		input:   input,
	}
}

func getHelp(args []string) Command {
	command := ""
	if len(args) > 2 {
		command = args[2]
	}

	return Help{
		command: command,
	}
}

// Do - for error.
func (errorMsg ErrorMsg) Do() {
	log.Fatalf("ERROR: %s", errorMsg.message)
}

// Do - for help.
func (help Help) Do() {
	log.SetFlags(0)
	switch help.command {
	case "decode":
		log.Println(`Get data from image:
	decode <path> [-o="path-to-save"]
		path - path to image to get data from
		-o - path to save content. Without will print content to STDOUT`)
	case "encode":
		log.Println(`Write data to image:
	encode <path> [-o="path-to-output"] [-i="path-to-save"] <message>
		path - path to source image
		-o - path to result, result.png if none
		-i - path to save. If it present program will use file content instead of image 
		message - message`)
	case "capacity":
		log.Println(`Get capacity of image:
		capacity <path>
		path - path to image`)
	default:
		log.Println(`Write bytes into image
	Commands:
		help <command> - info
		decode <path> - get data
		encode <path> <message> - write data
		capacity <path> - get capacity of image in bytes`)
	}
}

func (encode Encode) Do() {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)

	file, err := os.Open(encode.path)
	if err != nil {
		log.Printf(`ERROR: can't open file
	%s`, err)

		return
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		log.Printf(`ERROR: can't read file
	%s`, err)

		return
	}

	message, err := encode.getMessage()
	if err != nil {
		log.Printf(`ERROR: can't read input
	%s`, err)

		return
	}
	encImg := encimage.GetPixels(img)
	encImg.InitToWrite()
	_, err = encImg.Write(message)
	if err != nil {
		log.Printf(`ERROR: can't write bytes
	%s`, err)

		return
	}

	newFile, err := os.Create(encode.output)
	if err != nil {
		log.Printf(`ERROR: can't write file
	%s`, err)

		return
	}
	defer newFile.Close()
	newImage := encImg.GetImage()
	err = png.Encode(newFile, newImage)
	if err != nil {
		log.Printf(`ERROR: can't write result
	%s`, err)

		return
	}
}

func (decode Decode) Do() {
	encimage := getEncodeImage(decode.path)
	if encimage == nil {
		return
	}

	buff, err := io.ReadAll(encimage)
	if err != nil && !errors.Is(err, io.EOF) {
		log.Printf(`ERROR: can't read file
	%s`, err)

		return
	}

	decode.message = buff

	err = decode.out()
	if err != nil {
		log.Printf(`ERROR: can't write file
	%s`, err)
	}
}

func (capacity Capacity) Do() {
	encimage := getEncodeImage(capacity.path)
	if encimage == nil {
		return
	}

	log.SetFlags(0)
	log.Println(encimage.GetCapacity())
}

func getEncodeImage(path string) *encimage.Image {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	file, err := os.Open(path)
	if err != nil {
		log.Printf(`ERROR: can't open file
	%s`, err)

		return nil
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		log.Printf(`ERROR: can't read file
	%s`, err)

		return nil
	}
	encimage := encimage.GetPixels(img)

	return &encimage
}

func (encode Encode) getMessage() ([]byte, error) {
	if encode.input == "" {
		return encode.message, nil
	}

	file, err := os.Open(encode.input)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	message, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return message, nil
}

func (decode Decode) out() error {
	if decode.output == "" {
		log.SetFlags(0)
		log.Println(string(decode.message))

		return nil
	}

	file, err := os.Create(decode.output)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(decode.message)

	return err
}
