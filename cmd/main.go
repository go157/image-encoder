package main

import (
	"gitlab.com/ffsaschagff/imgencoder/internal/configcli"
)

func main() {
	config := configcli.GetConfig()
	config.Command.Do()
}
